<?php

namespace App\Controllers;

use App\Core\App;

class PagesController
{
    public function home()
    {
        $users = App::get('database')->selectAll('users');

        require view('index', [
            'users' => $users
        ]);
    }

    public function about()
    {
        require view('about');
    }

    public function contact()
    {
        require view('contact');
    }
}
